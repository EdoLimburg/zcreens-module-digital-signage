const _ = require('underscore');
const mongoose = require('mongoose');
const config = require('../../config/environment');

const Asset = mongoose.model('Asset');

const convertGrapesjsAsset = (asset) => ({
  type: 'image',
  src: `${config.url}/assets/${asset._id}/${asset.originalName}`,
});

/**
 * Called when an image is uploaded.
 * The image is already written to the filesystem.
 * This method will save the name and location
 *  to the database so it can be retrieved when needed.
 * @param req
 * @param res
 */
exports.uploadAsset = function (req, res) {
  if (req.files) {
    const newDocs = [];
    const errors = [];
    Promise.all(req.files.map(file => new Promise((resolve) => {
      const asset = new Asset({
        originalName: file.originalname,
        fileName: file.filename,
        mimeType: file.mimetype,
        path: file.path,
      });
      asset.updateSave((err, doc) => {
        if (err || doc === null) {
          errors.push(err);
        } else {
          newDocs.push(doc);
        }
        resolve();
      });
    }))).then(() => {
      if (errors.length > 0 || newDocs.length === 0) {
        console.error(JSON.stringify(errors));
        res.json({ success: false, msg: errors });
      } else {
        res.status(201).json({ data: newDocs.map(convertGrapesjsAsset) });
      }
    }).catch(e => {
      res.status(500).json({ success: false, msg: e });
    });
  }
};

/**
 * This method is called when an image is requested.
 * It will read the location from the database
 * and return the image that is linked to it.
 * @param req
 * @param res
 */
exports.getById = function (req, res) {
  Asset.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.sendFile(`${doc.path}`);
    }
  });
};
