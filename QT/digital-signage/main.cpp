#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <qtwebengineglobal.h>
#include <QWebEngineView>
#include <QFileInfo>
#include "websocket.h"
#include "enginemanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QtWebEngine::initialize();

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    bool debug = true;


    QObject* webEngine= engine.rootObjects().first()->findChild<QObject*>("webEngine");

    QString serverUrl = "http://digital-signage.edolimburg.nl/scenes/view/";

    EngineManager *engineManager = new EngineManager(serverUrl);
    engineManager->setEngine(webEngine);

    //webEngine->setProperty("url", QUrl::fromLocalFile(QFileInfo("index.html").absoluteFilePath()));


    WebSocket client(QUrl(QStringLiteral("ws://digital-signage.edolimburg.nl:9000")), debug);
    QObject::connect(&client, &WebSocket::closed, &app, &QCoreApplication::quit);
    QObject::connect(&client, &WebSocket::urlUpdated, engineManager, &EngineManager::updateUrl);

    return app.exec();
}


