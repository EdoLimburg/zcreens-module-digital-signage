// Example model

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TemplateSchema = new Schema({
  /** The name of the template */
  name: String,
  /** The CSS content of the template */
  css : String,
  /** The HTML content of the template */
  html: String,
});

TemplateSchema.methods = {
  updateSave(cb) {
    const self = this;
    this.validate((err) => {
      if (err) return cb(err);
      return self.save(cb);
    });
  },
};
TemplateSchema.statics = {
  get(sceneId, cb) {
    this.findOne({ _id: sceneId })
      .exec(cb);
  },
  getAll(cb) {
    this.find({})
      .sort({ name: -1 }) // sort by date
      .exec(cb);
  },
};

module.exports = mongoose.model('Template', TemplateSchema);
