#include "websocket.h"
#include <QtCore/QDebug>

QT_USE_NAMESPACE

WebSocket::WebSocket(const QUrl &url, bool debug, QObject *parent) :
    QObject(parent),
    m_url(url),
    m_debug(debug)
{
    if (m_debug)
        qDebug() << "WebSocket server:" << url;
    connect(&m_webSocket, &QWebSocket::connected, this, &WebSocket::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &WebSocket::reconnect);

    m_webSocket.open(QUrl(url));
}

void WebSocket::reconnect()
{
    m_webSocket.open(QUrl(""));
}

void WebSocket::onConnected()
{
    if (m_debug)
        qDebug() << "WebSocket connected";
    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &WebSocket::onTextMessageReceived);
    m_webSocket.sendTextMessage(QStringLiteral("name:MyFirstClient"));
}

void WebSocket::onTextMessageReceived(QString message)
{
    if (m_debug)
        qDebug() << "Message received:" << message;

    if(message.startsWith("playScene"))
    {
     emit urlUpdated(message.split(":")[1]);
    }
 //   m_webSocket.close();
}
