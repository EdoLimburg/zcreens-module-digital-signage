const _ = require('underscore');
const mongoose = require('mongoose');
const { ObjectID } = require('mongodb');

const Scene = mongoose.model('Scene');

/**
 * This method is used to prepare the data for viewing a shot.
 * It includes the sceneData, the next shot,
 * how long the shot should play
 * and how long the scrolling should take.
 * @param scene
 * @param shot
 * @param nextShot
 */
const createWindowVariables = (scene, shot, nextShot) => `
  window.shots = [${scene.shots.map(x => `' ${x._id}: ${x.length}'`)}]; 
  window.sceneId = '${scene._id}'; 
  window.nextShot = '${nextShot}'; 
  window.shotLength = '${shot.length}'
  window.scrollTime = '${shot.scrollTime}'
  window.shotId = '${shot._id}'
  `;

/**
 * This method returns a list with all the scenes
 * @param req
 * @param res
 */
exports.getAll = function (req, res) {
  Scene.getAll((err, docs) => {
    if (err || docs === null) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.json(docs);
    }
  });
};

/**
 * This method is used to create a new scene based on the data from the request.
 * It saves the new scene to the database and returns the new scene.
 * @param req
 * @param res
 */
exports.postScene = function (req, res) {
  const sceneData = _.extend(req.body);
  const scene = new Scene(sceneData);
  scene.updateSave((err, doc) => {
    if (err || doc === null) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.status(201).json({ success: true, msg: scene });
    }
  });
};

/**
 * This method returns the scene with the requested id.
 * @param req
 * @param res
 */
exports.getById = function (req, res) {
  Scene.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.json({ success: true, msg: doc });
    }
  });
};

/**
 * This method removes the scene with requested id from the database.
 * @param req
 * @param res
 */
exports.rmById = function (req, res) {
  Scene.findByIdAndRemove({ _id: req.params.id }, (err, doc) => {
    if (err || doc === null) {
      res.status(404).json({ success: false });
    } else {
      res.status(204).json({ success: true, msg: 'remove by id' });
    }
  });
};

/**
 * This method removes the shot with requested id from the scene.
 * @param req
 * @param res
 */
exports.rmShotById = function (req, res) {
  Scene.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      doc.shots = doc.shots.filter(x => !x._id.equals(new ObjectID(req.params.shotId)));
      doc.updateSave((err1, savedDoc) => {
        if (err1 || savedDoc === null) {
          console.error(err1);
          res.json({ success: false, msg: err1 });
        } else {
          res.status(204).json({ success: true, msg: savedDoc });
        }
      });
    }
  });
};

/**
 * This method adds a new shot with given name to the scene.
 * @param req
 * @param res
 */
exports.postShot = function (req, res) {
  const shotData = _.extend(req.body);
  Scene.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      doc.shots.push(shotData);
      doc.updateSave((err1, savedDoc) => {
        if (err1 || savedDoc === null) {
          console.error(err1);
          res.json({ success: false, msg: err1 });
        } else {
          res.status(201).json({ success: true, msg: savedDoc });
        }
      });
    }
  });
};

/**
 * This method updates the shot with given content.
 * The following properties can be updated:
 *  - html
 *  - css
 *  - length
 *  - scrollTime
 * @param req
 * @param res
 */
exports.updateShot = function (req, res) {
  const shotData = _.extend(req.body);
  Scene.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      const shot = doc.shots.find(d => d._id.equals(new ObjectID(shotData._id)));
      if (shot) {
        shot.html = shotData.html;
        shot.css = shotData.css;
        shot.length = shotData.length;
        shot.scrollTime = shotData.scrollTime;

        doc.updateSave((err1, savedDoc) => {
          if (err1 || savedDoc === null) {
            console.error(err1);
            res.json({ success: false, msg: err1 });
          } else {
            res.status(201).json({ success: true, msg: savedDoc });
          }
        });
      } else {
        res.status(404).json({
          success: false,
          msg: `could not find shot with id: ${shotData._id}`,
        });
      }
    }
  });
};

/**
 * This method is used by the clients to request the content of a shot when they have to play it.
 * The next shot is determined as well so the client can easily get the next shot content as well.
 * When the requested shot doesn't exist anymore it will return the first shot so the scene starts from the beginning.
 * @param req
 * @param res
 */
exports.viewShot = function (req, res) {
  Scene.get(req.params.id, (err, scene) => {
    if (err) {
      console.error(err);
      res.status(500).json({ success: false, msg: err });
    } else if (!scene) {
      res.status(404).json({ success: false, msg: `Could not find scene ${req.params.id}` });
    } else {
      if (scene.shots.length > 0) {
        let shot = scene.shots[0];
        if (req.params.shotId) {
          try {
            const newShot = scene.shots.find(x => x._id.equals(new ObjectID(req.params.shotId)));
            if (newShot) {
              shot = newShot;
            }
          } catch (e) {
            console.log(e);
          }
        }
        let index = scene.shots.indexOf(shot) + 1;
        if (index > scene.shots.length - 1) {
          index = 0;
        }
        shot.length = shot.length || 0;
        const nextShot = scene.shots[index]._id;
        return res.render('scene.pug', {
          windowVariables: createWindowVariables(scene, shot, nextShot),
          scene,
          shot,
          nextShot,
        });
      }
      res.status(404).json({
        success: false,
        msg: `could not find any shots for: ${req.params.id}`,
      });
    }
  });
};
