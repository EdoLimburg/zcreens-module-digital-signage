const _ = require('underscore');
const mongoose = require('mongoose');

const Template = mongoose.model('Template');

/**
 * This method returns a list with all the templates
 * @param req
 * @param res
 */
exports.getAll = function (req, res) {
  Template.getAll((err, docs) => {
    if (err || docs === null) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.json(docs);
    }
  });
};

/**
 * This method creates a new template from the posted data.
 * @param req
 * @param res
 */
exports.postTemplate = function (req, res) {
  const templateData = _.extend(req.body);
  const template = new Template(templateData);
  template.updateSave((err, doc) => {
    if (err || doc === null) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.status(201).json({ success: true, msg: template });
    }
  });
};

/**
 * This method updates the template with the updated content.
 * The following properties can be updated:
 *  - html
 *  - css
 * @param req
 * @param res
 */
exports.updateTemplate = function (req, res) {
  Template.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      const updatedTemplate = _.extend(doc, req.body);
      updatedTemplate.updateSave((err, doc) => {
        if (err) {
          console.error(err);
          res.json({ success: false, msg: err });
        } else {
          res.json({ success: true, msg: doc });
        }
      });
    }
  });
};

/**
 * This method returns the scene with requested id.
 * @param req
 * @param res
 */
exports.getById = function (req, res) {
  Template.get(req.params.id, (err, doc) => {
    if (err) {
      console.error(err);
      res.json({ success: false, msg: err });
    } else {
      res.json({ success: true, msg: doc });
    }
  });
};

/**
 * This method removes the template with requested id.
 * @param req
 * @param res
 */
exports.rmById = function (req, res) {
  Template.findByIdAndRemove({ _id: req.params.id }, (err, doc) => {
    if (err || doc === null) {
      res.status(404).json({ success: false });
    } else {
      res.status(204).json({ success: true, msg: 'remove by id' });
    }
  });
};
