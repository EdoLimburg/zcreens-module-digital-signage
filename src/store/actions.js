import * as types from './mutation_types.js';
import Vue from 'vue';

export default {
  /**
   * Retrieve all scenes from the api and commit the to the state.
   * @param commit
   */
  searchScenes({ commit }) {
    Vue.http.get('scenes').then((res) => {
      res.json().then((data) => {
        commit(types.SEARCH_SCENES, data);
      });
    });
  },

  /**
   * Retrieve the selected scene and commit it to the state.
   * @param commit
   * @param sceneId
   */
  getSelectedScene({ commit }, sceneId) {
    Vue.http.get(`scenes/${sceneId}`).then((res) => {
      res.json().then((data) => {
        commit(types.SELECTED_SCENE, data.msg);
      });
    });
  },

  /**
   * Save the new scene and commit it to the state.
   * @param commit
   * @param newScene
   */
  addScene({ commit }, newScene) {
    Vue.http.post('scenes', newScene).then((res) => {
      res.json().then((data) => {
        commit(types.ADD_SCENE, data.msg);
      });
    });
  },

  /**
   * Delete the scene from the api and commit to the state.
   * @param commit
   * @param sceneId
   */
  removeScene({ commit }, sceneId) {
    Vue.http.delete(`scenes/${sceneId}`).then(() => {
      commit(types.REMOVE_SCENE, sceneId);
    });
  },

  /**
   * Save the shot to the api and commit to the state.
   * @param dispatch
   * @param sceneId
   * @param shot
   */
  addShot({ dispatch }, { sceneId, shot }) {
    Vue.http.post(`scenes/${sceneId}/shot`, shot).then((res) => {
      res.json().then(() => {
        dispatch('getSelectedScene', sceneId);
      });
    });
  },

  /**
   * Remove the shot from the api and commit to the state.
   * @param commit
   * @param sceneId
   * @param shotId
   */
  removeShot({ commit }, { sceneId, shotId }) {
    Vue.http.delete(`scenes/${sceneId}/shot/${shotId}`).then(() => {
      commit(types.REMOVE_SHOT, { sceneId, shotId });
    });
  },

  /**
   * Update the shot in the backend.
   * @param store
   * @param sceneId
   * @param shot
   */
  updateShot(store, { sceneId, shot }) {
    Vue.http.put(`scenes/${sceneId}/shot`, shot);
  },

  /**
   * Retrieve all clients from the api and commit to the state.
   * @param commit
   */
  searchClients({ commit }) {
    Vue.http.get('clients').then((res) => {
      res.json().then(data => {
        commit(types.SEARCH_CLIENTS, data);
      });
    });
  },

  /**
   * Retrieve all clients from the api and commit to teh state.
   * @param commit
   */
  searchTemplates({ commit }) {
    Vue.http.get('templates').then((res) => {
      res.json().then((data) => {
        commit(types.SEARCH_TEMPLATES, data);
      });
    });
  },

  /**
   * Save the template to the api and commit to the state
   * @param commit
   * @param template
   */
  addTemplate({ commit }, template) {
    Vue.http.post('templates', template).then((res) => {
      res.json().then((data) => {
        commit(types.ADD_TEMPLATE, data.msg);
      });
    });
  },

  /**
   * Retrieve the selected template from the api and commit to the state.
   * @param commit
   * @param templateId
   */
  getSelectedTemplate({ commit }, templateId) {
    Vue.http.get(`templates/${templateId}`).then((res) => {
      res.json().then((data) => {
        commit(types.SELECTED_TEMPLATE, data.msg);
      });
    });
  },

  /**
   * Save the template to the api and commit to the state.
   * @param store
   * @param template
   */
  updateTemplate(store, { template }) {
    Vue.http.put(`templates/${template._id}`, template).then((res) => {

    });
  },

  /**
   * Remove the template from the api and commit to the state.
   * @param commit
   * @param templateId
   */
  removeTemplate({ commit }, templateId) {
    Vue.http.delete(`templates/${templateId}`).then(() => {
      commit(types.REMOVE_TEMPLATE, templateId);
    });
  }
};
