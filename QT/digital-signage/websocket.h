#ifndef ECHOCLIENT_H
#define ECHOCLIENT_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>

class WebSocket : public QObject
{
    Q_OBJECT
public:
    explicit WebSocket(const QUrl &url, bool debug = false, QObject *parent = Q_NULLPTR);

Q_SIGNALS:
    void closed();
    void urlUpdated(QString &url);

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    void reconnect();

private:
    QWebSocket m_webSocket;
    QUrl m_url;
    bool m_debug;
};

#endif // ECHOCLIENT_H
