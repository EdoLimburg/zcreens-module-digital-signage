const express = require('express');
const controller = require('./templates.controller');

const router = express.Router();

router.get('/', controller.getAll);
router.post('/', controller.postTemplate);
router.get('/:id', controller.getById);
router.put('/:id', controller.updateTemplate);
router.delete('/:id', controller.rmById);

module.exports = router;
