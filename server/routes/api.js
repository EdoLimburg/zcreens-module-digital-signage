/**
 * Main application routes.
 */

module.exports = function (app) {
  app.use('/scenes', require('./../api/scenes/index'));
  app.use('/clients', require('./../api/clients/index'));
  app.use('/templates', require('./../api/templates/index'));
  app.use('/assets', require('../api/assets/index'));
};
