// Development specific configuration
// ==================================
module.exports = {
  db: {
    mysql: {
      host: 'localhost',
      dialect: 'mysql', // 'mysql'|'mariadb'|'sqlite'|'postgres'|'mssql'
      pool: {
        max: 5,
        min: 0,
        idle: 10000,
      },
      user: 'root',
      password: '',
      database: 'test',
    },
    mongo: {
      uri: 'mongodb://mongo/test',
    },
  },
  redis: {
    port: 6379,
    host: 'redis',
    options: {
      detect_buffers: true,
    },
  },
  appPath: 'dist',
  seedDB: true,
  root: '/app',
  url: 'http://digital-signage.edolimburg.nl',
};
