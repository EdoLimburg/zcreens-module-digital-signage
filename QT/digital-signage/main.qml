import QtQuick 2.8
import QtQuick.Window 2.2
import QtWebEngine 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    WebEngineView {
            objectName: "webEngine"
            anchors.fill: parent
        }
}
