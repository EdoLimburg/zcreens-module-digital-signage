window.onload = function () {
  // Get all variables from the Html
  var shotId = window.shotId;
  var sceneId = window.sceneId;
  var length = parseInt(window.shotLength, 10);
  var nextShot = window.nextShot;
  var scrollTime = parseInt(window.scrollTime);


  // wait for interval to pass and navigate to the new shot.
  if (isNaN(length) || length > 0) {
    setTimeout(function() {
      window.location = '/scenes/view/' + sceneId + '/shot/' + nextShot;
    }, length * 1000);
  }

  /**
   * Auto scroll to the bottom in given time.
   */
  if (!isNaN(scrollTime) && scrollTime > 0) {
    var totalScroll = document.getElementById(shotId).scrollHeight - window.innerHeight;
    var scrollSteps = totalScroll / (scrollTime * 100);
    var currentScroll = 0;
    setInterval(function () {
      window.scroll(0, currentScroll += scrollSteps);
    }, 10);
  }
};
