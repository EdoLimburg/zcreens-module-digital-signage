'use strict';

var express = require('express');
var controller = require('./clients.controller');
var router = express.Router();

router.get('/', controller.getAll);
router.post('/play', controller.playScene);
module.exports = router;
