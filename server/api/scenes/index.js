

const express = require('express');
const controller = require('./scenes.controller');

const router = express.Router();

router.get('/', controller.getAll);
router.post('/', controller.postScene);
router.get('/:id', controller.getById);
router.delete('/:id', controller.rmById);
router.post('/:id/shot', controller.postShot);
router.put('/:id/shot', controller.updateShot);
router.delete('/:id/shot/:shotId', controller.rmShotById);
router.get('/view/:id', controller.viewShot);
router.get('/view/:id/shot/:shotId', controller.viewShot);


module.exports = router;
