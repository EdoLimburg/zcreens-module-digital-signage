/**
 * Default configuration for the GrapesJS AssetManager
 * @type {{assets: Array, upload: string, storageType: string, storeOnChange: boolean, storeAfterUpload: boolean}}
 */
export const assetManagerConfig = {
  assets: [],
  upload: '/assets/upload',
  storageType: '',
  storeOnChange: true,
  storeAfterUpload: true,
};

/**
 * Add default configuration to the editor
 * @param editor: The GrapesJS editor
 * @param fnSaveChanges Function: The method to call when the save button is clicked on.
 */
export const configureEditor = (editor, fnSaveChanges) => {
  const deviceManager = editor.DeviceManager;

  // remove existing devices as we only need horizontal and portrait.
  const devices = deviceManager.getAll().map(x => x);
  devices.forEach(x => deviceManager.getAll().remove(x));
  deviceManager.add('horizontal', '');
  deviceManager.add('portrait', '1080px');

  // add save button to the editor
  editor.Panels.addButton(
    'options',
    [
      {
        id: 'save-db',
        className: 'fa fa-floppy-o',
        command: 'save-db',
        attributes: { title: 'Save Changes' },
        pluginsOpts: {
          'gjs-blocks-basic': { /* ...options */ },
        },
      },
    ],
  );

  // add a listener on the editor which will run when the save changes button is clicked
  editor.Commands.add(
    'save-db',
    {
      run: (e, sender) => {
        fnSaveChanges();
        sender.set('active', false);
      },
    },
  );

  const domc = editor.DomComponents;
  const defaultType = domc.getType('default');
  const defaultModel = defaultType.model;
  const defaultView = defaultType.view;

  domc.addType('iframe', {
    model: defaultModel.extend(
      {
        defaults: Object.assign(
          {}, defaultModel.prototype.defaults,
          {
            traits: ['src'],
          },
        ),
      },
      {
        // eslint-disable-next-line consistent-return
        isComponent: (el) => {
          if (el.tagName && el.tagName.toLowerCase() === 'iframe') {
            return { type: 'iframe' };
          }
        },
      },
    ),
    view: defaultView,
  });
};
