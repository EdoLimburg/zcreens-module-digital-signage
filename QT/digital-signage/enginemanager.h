#ifndef ENGINEMANAGER_H
#define ENGINEMANAGER_H

#include <QObject>
#include <QGuiApplication>

class EngineManager : public QObject
{
    Q_OBJECT
public:
    explicit EngineManager(QString &serverUrl, QObject *parent = Q_NULLPTR);
public Q_SLOTS:
    void setEngine(QObject* engine);
    void updateUrl(QString &url);

private:
    QObject* engine;
    QString serverUrl;
};

#endif // ENGINEMANAGER_H
