<template>
  <div grid-list-md grid-list-xl fluid="true">
    <v-toolbar>
      <v-toolbar-title v-if="template">{{template.name}}</v-toolbar-title>
      <v-toolbar-items class="hidden-sm-and-down">
        <v-btn flat v-on:click="removeTemplate()">Template Verwijderen</v-btn>
      </v-toolbar-items>
    </v-toolbar>

    <v-layout row wrap>

      <v-flex xs12 style="margin-left:3em;">
        <div style="height: calc(100vh - 10em); " id="template-editor"></div>
      </v-flex>
    </v-layout>
  </div>
</template>

<style>

</style>

<script>
  import { mapState } from 'vuex'
  import grapesjs from 'grapesjs';
  import basic_block from 'grapesjs-blocks-basic';
  import 'grapesjs/dist/css/grapes.min.css';
  import { assetManagerConfig, configureEditor } from '../grapesjs.config';

  export default {
    props: ['id'],
    data() {
      return {
      }
    },
    methods: {
      /**
       * Dispatches an action to get the selected template
       */
      getData() {
        this.$store.dispatch('getSelectedTemplate', this.id);
      },

      /**
       * When the same editor instance is used for editing different templates
       * the editor should be updated with the new content.
       */
      updateEditor: function () {
        if (this.template) {
          if (this.template.html !== '') {
            this.editor.setComponents(this.template.html);
          }
          if (this.template.css && this.template.css !== '') {
            this.editor.setStyle(this.template.css);
          }
        }
      },
      /**
       * Dispatches an action to remove the template. Navigate back to the templates list.
       */
      removeTemplate(){
        this.$store.dispatch('removeTemplate', this.id);
        this.$router.replace('/templates');
      },
      /**
       * Dispatch action to save the content changes to the database.
       */
      saveChanges() {
        if (this.template) {
          this.template.html = this.editor.getHtml();
          this.template.css = this.editor.getCss();
          this.$store.dispatch('updateTemplate', {
            template: this.template
          });
        }
      }
    },
    watch: {
      /**
       * Update the editor when the template changes
       */
      template: function (val) {
        if (val) {
          this.updateEditor();
        }
      }
    },
    /**
     * Computed property needed for VueX store. The template property will be updated when the state changes
     */
    computed: mapState({
      template: state => state.selectedTemplate
    }),

    /**
     * Create a new GrapesJS editor when this component is mounted.
     */
    mounted: function () {
      this.editor = grapesjs.init({
        container: '#template-editor',
        height: this.height,
        plugins: ['gjs-blocks-basic'],
        assetManager: assetManagerConfig
      });

      configureEditor(this.editor, () => this.saveChanges());

      // if the shot is already set the content of that shot should be set on the editor
      if (this.template) {
        this.updateEditor();
      }

      this.getData();
    },
  }
</script>
