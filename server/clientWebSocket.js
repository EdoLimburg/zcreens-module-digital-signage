const WebSocketServer = require('ws').Server;
const uuidv4 = require('uuid/v4');

let wsServer;
let clients = [];

/**
 * Initializes the socket on the given server.
 * This socket will accept connections from clients that can play the scenes.
 * The clients are saved to the clients array so they can be controlled from the webapp.
 * When the client disconnects the client is removed from the clients so the user cannot select it anymore.
 * @param server
 */
exports.initSocket = function (server) {
  wsServer = new WebSocketServer({
    server,
    autoAcceptConnections: true,
  });

  wsServer.on('connection', (connection) => {
    connection.on('close', () => {
      clients = clients.filter(x => x.ws !== connection);
      console.log(`connection lost ${clients.length}`);
    });

    connection.on('message', (data) => {
      /**
       * Only add the client after the name is set. After that it will be visible in the webapp.
       */
      if (data.startsWith('name')) {
        clients.push({
          name: data.split(':')[1],
          ws: connection,
          id: uuidv4(),
        });

        console.log(`new connection ${clients.length}`);
      }
    });
    connection.on('error', (err) => {
      clients = clients.filter(x => x.ws !== connection);
      console.log(`connection lost ${clients.length} due to error: ${err}`);
    });
  });
};

/**
 * This method sends the playScene message to the clients that are selected.
 * @param sceneId
 * @param clientIds
 */
exports.playScene = (sceneId, clientIds) => {
  console.log(JSON.stringify(clientIds));
  clients.filter(x => clientIds.indexOf(x.id) > -1).forEach(x => {
    x.ws.send(`playScene:${sceneId}`);
  });
};

/**
 * This method returns all connected clients
 * @returns {Array}
 */
exports.getClients = () => {
  return clients.map(x => ({ id: x.id, name: x.name }));
};
