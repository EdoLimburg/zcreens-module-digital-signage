// Example model

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SceneSchema = new Schema({
  /** The name of the scene*/
  name : String,
  shots: [
    {
      /** The name of the shot */
      name      : String,
      /** The HTML content of the shot */
      html      : String,
      /** The CSS content of the shot */
      css       : String,
      /** The time this shot should play */
      length    : String,
      /** The time it should take to scroll to the bottom */
      scrollTime: String,
    }
  ],
}, { usePushEach: true });

SceneSchema.methods = {
  updateSave(cb) {
    const self = this;
    this.validate((err) => {
      if (err) return cb(err);
      return self.save(cb);
    });
  },
};
SceneSchema.statics = {
  get(sceneId, cb) {
    this.findOne({ _id: sceneId })
      .exec(cb);
  },
  getAll(cb) {
    this.find({})
      .sort({ name: -1 }) // sort by date
      .exec(cb);
  },
};

module.exports = mongoose.model('Scene', SceneSchema);
