import SceneList from '../components/SceneList.vue';
import Scene from '../components/Scene.vue';
import TemplateList from '../components/TemplateList.vue';
import Template from '../components/Template.vue';

export default {
  routes: [
    {
      path: '/',
      redirect: '/scenes',
    },
    {
      path: '/scenes',
      component: SceneList,
    },
    {
      path: '/scene/:id',
      component: Scene,
      props: true,
    },
    {
      path: '/templates',
      component: TemplateList,
    },
    {
      path: '/template/:id',
      component: Template,
      props: true,
    },
  ],
};

