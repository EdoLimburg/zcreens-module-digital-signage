// Example model

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AssetSchema = new Schema({
  /** The name it had when before uploading. */
  originalName: String,
  /** The new generated name it got when written to the filesystem */
  fileName: String,
  /** The mimetype of the file */
  mimeType: String,
  /** The filePath to the uploaded file. Used to return the file when requested */
  path: String,
});

AssetSchema.methods = {
  updateSave(cb) {
    const self = this;
    this.validate((err) => {
      if (err) return cb(err);
      return self.save(cb);
    });
  },
};
AssetSchema.statics = {
  get(sceneId, cb) {
    this.findOne({ _id: sceneId })
      .exec(cb);
  },
  getAll(cb) {
    this.find({})
      .sort({ name: -1 }) // sort by date
      .exec(cb);
  },
};

module.exports = mongoose.model('Asset', AssetSchema);
