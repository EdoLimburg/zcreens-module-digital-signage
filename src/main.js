// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import Vuetify from 'vuetify';
import store from './store';
import App from './App';
import routes from './router/index';

// import './util/mock.js'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuetify);
const router = new VueRouter(routes);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  ...App,
});
