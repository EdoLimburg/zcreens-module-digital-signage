const express = require('express');
const controller = require('./assets.controller');

const router = express.Router();
const multer = require('multer');
const config = require('../../config/environment');
const path = require('path');

const upload = multer({ dest: path.join(config.root, config.appPath, 'uploads') });

router.post('/upload', upload.array('files[]'), controller.uploadAsset);
router.get('/:id/:originalName', controller.getById);
module.exports = router;
