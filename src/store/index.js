import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex);

const state = {
  /**
   * All scenes
   */
  scenes: [],
  /**
   * The current selected scene
   */
  selectedScene: {},
  /**
   * All connected clients
   */
  clients: [],
  /**
   * All templates
   */
  templates: [],
  /**
   * The current selected template
   */
  selectedTemplate: null,
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
});
