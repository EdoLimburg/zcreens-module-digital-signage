import * as types from './mutation_types';

export default {
  /**
   * Update state with scenes
   * @param state
   * @param scenes
   */
  [types.SEARCH_SCENES](state, scenes) {
    state.scenes = Array.from(scenes);
  },
  /**
   * Update state with selected scene
   * @param state
   * @param scene
   */
  [types.SELECTED_SCENE](state, scene) {
    state.selectedScene = scene;
  },
  /**
   * Add scene to the state.
   * @param state
   * @param scene
   */
  [types.ADD_SCENE](state, scene) {
    state.scenes = Array.from([...state.scenes, scene]);
  },
  /**
   * Remove scene from the state.
   * @param state
   * @param sceneId
   */
  [types.REMOVE_SCENE](state, sceneId) {
    state.scenes = Array.from(state.scenes.filter(x => x._id !== sceneId));
    if (state.selectedScene._id === sceneId) {
      state.selectedScene = {};
    }
  },
  /**
   * Remove shot from the scene.
   * @param state
   * @param sceneId
   * @param shotId
   */
  [types.REMOVE_SHOT](state, { sceneId, shotId }) {
    if (state.selectedScene._id === sceneId) {
      state.selectedScene = Object.assign(
        {}, state.selectedScene,
        { shots: Array.from(state.selectedScene.shots.filter(x => x._id !== shotId)) },
      );
    }
  },
  /**
   * Update state with clients
   * @param state
   * @param clients
   */
  [types.SEARCH_CLIENTS](state, clients) {
    state.clients = Array.from(clients);
  },

  /**
   * Update state with templates
   * @param state
   * @param templates
   */
  [types.SEARCH_TEMPLATES](state, templates) {
    state.templates = Array.from(templates);
  },

  /**
   * Add template to the state.
   * @param state
   * @param template
   */
  [types.ADD_TEMPLATE](state, template) {
    state.templates = Array.from([...state.templates, template]);
  },
  /**
   * Set the selected template
   * @param state
   * @param template
   */
  [types.SELECTED_TEMPLATE](state, template) {
    state.selectedTemplate = template;
  },
  /**
   * Remove the template from the state.
   * @param state
   * @param templateId
   */
  [types.REMOVE_TEMPLATE](state, templateId) {
    state.templates = Array.from(state.templates.filter(x => x._id !== templateId));
  },
};
