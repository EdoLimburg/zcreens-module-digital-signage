#include "enginemanager.h"
#include <QUrl>
#include <QtCore/QObject>
#include <QGuiApplication>
#include <QObject>
QT_USE_NAMESPACE

EngineManager::EngineManager(QString &serverUrl, QObject *parent) :
    QObject(parent)
{
    this->serverUrl = serverUrl;
}

void EngineManager::setEngine(QObject *engine){
    this->engine = engine;
}

/**
 * Update Url to new Url
 * @brief EngineManager::updateUrl
 * @param url
 */
void EngineManager::updateUrl(QString &url)
{
    QString newUrl = this->serverUrl + url;
    this->engine->setProperty("url", QUrl(newUrl));
}
