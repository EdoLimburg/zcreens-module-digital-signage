const _ = require('underscore');
const mongoose = require('mongoose');

const clientWebsocket = require('../../clientWebSocket');

/**
 * This method returns all connected clients.
 * These are the clients that can be used to play the scenes on.
 * @param req
 * @param res
 */
exports.getAll = function (req, res) {
  res.json(clientWebsocket.getClients());
};

/**
 * This method is called when the scene should be played on the selected clients.
 * The parameters include an array of the selected devices and the scene to play.
 * It will use the websocket to notify the clients to play the scene.
 * @param req
 * @param res
 */
exports.playScene = function (req, res) {
  const data = _.extend(req.body);
  if (data.sceneId && data.clients) {
    clientWebsocket.playScene(data.sceneId, data.clients.map(x => x.id));
    res.json({ success: true });
  } else {
    res.status(500).json({ success: false, msg: 'Invalid data' });
  }
};
